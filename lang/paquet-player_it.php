<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-player?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'player_description' => 'Ce plugin permet la lecture de sons.
Il ajoute des lecteurs flash adaptés aux formats .mp3.
Il agit sur tous les <code><docXX|player></code> insérés dans les textes, aussi bien que dans les squelettes.

-* Dans un texte <code><docXX|player></code> affiche un lecteur flash audio (plusieurs lecteurs au choix) ;
-* Dans un squelette <code>#MODELE{playliste}</code> permet d’afficher une playliste des derniers mp3


Questo plugin consente la riproduzione di suoni.
Aggiunge unità flash adatte ai formati mp3.
Agisce su tutti i <code><docXX|player></code> inseriti nei testi, oltre che nei template.

- * In un testo <code><docXX|player></code> mostra un flash player audio (diversi player tra cui scegliere);
- * In un template <code>#MODELE {playliste}</code> mostra una playlist degli ultimi mp3',
	'player_slogan' => 'Riproduci suoni in mp3'
);
